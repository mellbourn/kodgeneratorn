const mix = require('laravel-mix');
mix.disableNotifications();

mix.copyDirectory('resources/fonts', 'public/fonts');
mix.js('resources/js/password-generator.js', 'dist').vue();
mix.js('resources/js/password-tester.js', 'dist').vue();
mix.js('resources/js/menu-toggle.js', 'dist');
mix.sass('resources/sass/main.scss', 'dist').options({
    processCssUrls: false
})
.version();
