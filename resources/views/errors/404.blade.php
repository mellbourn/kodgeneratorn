@extends('layout', [
  'title' => '404',
  'description' => 'Sidan du letar efter kunde inte hittas.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>404 - Sidan finns inte</h1>
      <p class="preamble">Ledsen, sidan du letar efter finns inte. Eftersom du har kommit till kodgeneratorn.se vill du kanske...
        <ul>
          <li><a href="/">Hem</a></li>
          <li><a href="{{ route('what_is_a_strong_password') }}">Vad är ett starkt lösenord?</a></li>
          <li><a href="{{ route('how_are_the_passwords_created') }}">Hur skapas lösenorden?</a></li>

          <li><a href="{{ route('what_is_security') }}">Vad är säkerhet?</a></li>
          <li><a href="{{ route('test_your_password') }}">Testa ditt lösenord</a></li>
          <li><a href="{{ route('integrations_and_plugins') }}">Integrationer och plugins</a></li>
          <li><a href="{{ route('api_documentation') }}">API-dokumentation</a></li>
        </ul>
      </p>
    </div>
  </section>
@endsection
