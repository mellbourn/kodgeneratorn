import { createApp } from 'vue';
import PasswordGenerator from './components/PasswordGenerator.vue';
createApp(PasswordGenerator).mount('#password-generator');
