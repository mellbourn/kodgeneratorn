import { createApp } from 'vue';
import PasswordTester from './components/PasswordTester.vue';
createApp(PasswordTester).mount('#password-tester');
