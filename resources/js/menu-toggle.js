document.addEventListener('DOMContentLoaded', function() {
  var button = document.querySelector('.nav-toggle-button');
  button.addEventListener('click', function() {
    button.closest('header').classList.toggle('nav-open');
    document.body.classList.toggle('disable-scroll');
  });
});