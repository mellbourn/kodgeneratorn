<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\SystemController;
use Illuminate\Support\Facades\Route;

/**
 * Route namespaces are used for IndexNow logic.
 */

Route::group(['namespace' => 'public'], function() {
  Route::get('/', [Controller::class, 'index']);
  Route::get('/vad-ar-ett-starkt-losenord', [Controller::class, 'whatIsAStrongPassword'])->name('what_is_a_strong_password');
  Route::get('/hur-skapas-losenorden', [Controller::class, 'howAreThePasswordsCreated'])->name('how_are_the_passwords_created');
  Route::get('/vad-ar-sakerhet', [Controller::class, 'whatIsSecurity'])->name('what_is_security');
  Route::get('/testa-ditt-losenord', [Controller::class, 'testYourPassword'])->name('test_your_password');
  Route::get('/integrationer-och-plugins', [Controller::class, 'plugins'])->name('integrations_and_plugins');
  Route::get('/api-dokumentation', [Controller::class, 'apiDocumentation'])->name('api_documentation');

  // Old API routes, redirect to new.
  Route::get('/generate', function() {
    return Redirect::to('/api/generate', 301);
  });

  Route::post('/analyze', function() {
    return Redirect::to('/api/analyze', 301);
  });
});

Route::group(['namespace' => 'api', 'prefix' => 'api'], function() {
  Route::get('/generate', [Controller::class, 'generate']);
  Route::post('/analyze', [Controller::class, 'analyze']);
});


// System routes
Route::group(['namespace' => 'system'], function() {
  Route::get('/system/import', [SystemController::class, 'import']);
  Route::get('/system/indexnow', [SystemController::class, 'indexNow']);
  Route::get('/indexnow.txt', function() {
    return response(config('services.indexnow.api_key'), 200, [
      'Content-Type' => 'application/txt'
    ]);
  });
});