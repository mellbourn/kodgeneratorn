<?php

namespace App\Providers;

use App\Services\PasswordService;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
     /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
        'PasswordService' => PasswordService::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
