<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ZxcvbnPhp\Zxcvbn;

// https://github.com/bjeavons/zxcvbn-php
// https://www.omnicalculator.com/other/password-entropy

class PasswordService
{
  /**
   * @var string
   */
  private $specialChars = '!\"#$%&\'()*+,-./:;=?@[\]^_`{|}~';

  /**
   * Undocumented function
   *
   * @param Request $request
   * @return void
   */
  public function generate(Request $request)
  {
    $numberOfWords = $request->number_of_words ?? 3;
    $useNumbers = (bool) $request->numbers ?? false;
    $numberOfNumbers = 0;
    $useSpecialChars =  (bool) $request->special_chars ?? false;
    $numberOfSpecialChars = 0;
    $useSwedishChars = (bool) $request->swedish_chars ?? true;

    if ($useNumbers && $request->number_of_numbers) {
      $numberOfNumbers = $request->number_of_numbers ?? 1;
    }

    if ($useSpecialChars && $request->number_of_special_chars) {
      $numberOfSpecialChars = $request->number_of_special_chars ?? 1;
    }

    // Calculate the shortest length of word
    $wordLength = 12 - $numberOfNumbers - $numberOfSpecialChars;
    $words = $this->getWords($numberOfWords, $wordLength);
    $originalWords = clone $words;

    $generated = $words->join('');

    // Handle logic for special chars between words.
    if ($useSpecialChars && $request->special_chars == 'between') {
      $specialChars = str_split($this->specialChars);
      for ($i = 0; $i < $numberOfWords - 1; $i++) {
        $randomSpecialChars = [];
        for ($index = 0; $index < $numberOfSpecialChars; $index++) {
          $randomSpecialChars[] = $specialChars[rand(0, count($specialChars) - 1)];
        }

        $words[$i] = $words[$i] . implode('', $randomSpecialChars);
      }

      $generated = $words->join('');
    }

    // Capitalize
    if ($request->capitalize == 'random') {
      // Random
      for ($index = 0; $index <= strlen($numberOfWords); $index++) {
        $randomIndex = rand(0, strlen($generated) - 1);
        $generated[$randomIndex] = strtoupper($generated[$randomIndex]);
      }
    } else if ($request->capitalize == 'word') {
      // ucfist
      $words = $words->map(function($word) {
        return mb_convert_case($word, MB_CASE_TITLE);
      });
      $generated = $words->join('');
    }

    // Special chars (start, end, random)
    if ($useSpecialChars && $request->special_chars != 'between') {
      $specialChars = str_split($this->specialChars);
      $randomSpecialChars = [];
      for ($index = 0; $index < $numberOfSpecialChars; $index++) {
        $randomSpecialChars[] = $specialChars[rand(0, count($specialChars) - 1)];
      }

      if ($request->special_chars == 'start') {
        $generated = join('', $randomSpecialChars) . $generated;
      } else if ($request->special_chars == 'end') {
        $generated = $generated . join('', $randomSpecialChars);
      } else if ($request->special_chars == 'random') {
        while (count($randomSpecialChars) > 0) {
          $randomChar = array_shift($randomSpecialChars);
          $generated = $this->stringInsert($generated, $randomChar, rand(0, strlen($generated)));
        }
      }
    }

    // Numbers (start, end, random)
    if ($useNumbers) {
      $randomNumbers = [];
      for ($index = 0; $index < $numberOfNumbers; $index++) {
        $randomNumbers[] = rand(0, 9);
      }

      if ($request->numbers == 'start') {
        $generated = join('', $randomNumbers) . $generated;
      } else if ($request->numbers == 'end') {
        $generated = $generated . join('', $randomNumbers);
      } else if ($request->numbers == 'random') {
        while (count($randomNumbers) > 0) {
          $randomNumber = array_shift($randomNumbers);
          $generated = $this->stringInsert($generated, $randomNumber, rand(0, strlen($generated)));
        }
      }
    }

    if (!$useSwedishChars) {
      $generated = $this->removeSwedishChars($generated);
    }

    // Generation is done. If you want to test with a really week password uncomment the line below.
    // $generated = 'password';

    $analysis = $this->analyze($generated);

    return [
      'source' => $originalWords,
      'generated' => $generated,
      'analysis' => $analysis,
    ];
  }

  /**
   * Analyze the strength of a password.
   *
   * @param string $password
   * @return array
   */
  public function analyze(string $password, int $numberOfWords = null)
  {
    // Strength info
    $analysis['length'] = strlen($password);
    $analysis['strength_explanations'] = [
      0 => [
        'header' => 'Svagt och väldigt lättgissat!',
        'body' => 'Det finns med stor sannolikhet i ordböcker som används vid ordboksattacker. Styrkan är %d/4. zxcvbn gav lösenordet %d poäng. Entropin är %f bitar.'
      ],
      1 => [
        'header' => 'Lättgissat!',
        'body' => 'Liknande lösenord finns i ordböcker som används vid ordboksattacker. Styrkan är %d/4. zxcvbn gav lösenordet %d poäng. Entropin är %f bitar.'
      ],
      2 => [
        'header' => 'Gissningsbart!',
        'body' => 'Ger visst skydd vid ohämmade attacker. Styrkan är %d/4. zxcvbn gav lösenordet %d poäng. Entropin är %f bitar.'
      ],
      3 => [
        'header' => 'Omöjligt att gissa!',
        'body' => 'Erbjuder måttligt skydd från offline slow-hash-scenarion. Styrkan är %d/4. zxcvbn gav lösenordet %d poäng. Entropin är %f bitar.'
      ],
      4 => [
        'header' => 'Galet starkt!',
        'body' => 'Omöjligt att gissa och ger ett starkt skydd mot offline slow-hash scenarion. Styrkan är %d/4. zxcvbn gav lösenordet %d poäng. Entropin är %f bitar.'
      ],
      'breached' => [
        'header' => 'Varning!',
        'body' => 'Det här lösenordet har dykt upp i läckor %d gånger'
      ]
    ];

    $zxcvbn = new Zxcvbn();
    $zxcvbn = $zxcvbn->passwordStrength(utf8_encode($password));
    $analysis['zxcvbn'] = $zxcvbn;

    $entropy = $this->calculateEntropy($password);
    $analysis['entropy'] = $entropy;

    // Compute combined score
    $score = floor(($zxcvbn['score'] + $entropy['score']) / 2);

    // Check HIBP
    $hibp = $this->getHIBPData($password);
    $analysis['breached'] = $hibp;

    // Exceptions that lower score.
    if ($hibp['breached']) {
      $score = 0;
      $analysisExplanationHeader = $analysis['strength_explanations']['breached']['header'];
      $analysisExplanationBody = sprintf($analysis['strength_explanations']['breached']['body'], $hibp['count']);
    } else {
      if ($numberOfWords == 1 && !$useSpecialChars && !$useNumbers) {
        // The use of one word is always easy to guess
        $score = 1;
      }

      $analysisExplanationHeader = $analysis['strength_explanations'][$score]['header'];
      $analysisExplanationBody = sprintf($analysis['strength_explanations'][$score]['body'], $score, $zxcvbn['score'], $entropy['value']);
    }

    $analysis['score'] = $score;
    $analysis['strength_explanation'] = [
      'header' => $analysisExplanationHeader,
      'body' => $analysisExplanationBody,
    ];

    return $analysis;
  }

  /**
   * Check Have I Been Pawned if password has been breached.
   *
   * @var string $string
   * @return array
   */
  private function getHIBPData(string $string)
  {
    $breached = false;
    $count = 0;

    try {
      $client = new Client([
        'headers' => [
          'hibp-api-key' => config('services.hibp.api_key')
        ]
      ]);
      $hash = strtoupper(sha1($string));
      $res = $client->request('GET', 'https://api.pwnedpasswords.com/range/' . substr($hash, 0, 5));
      $hibpHashes = (string) $res->getBody();

      if ($hibpHashes) {
        $hibpHashesArray = explode("\r\n", $hibpHashes);
        foreach ($hibpHashesArray as $hibpHash) {
          if (!empty($hibpHash)) {
            list($hibpHash, $count) = explode(':', $hibpHash);

            if ($hibpHash == substr($hash, 5)) {
              $breached = true;
              $count = $count;
              break;
            }
          }
        }
      }
    } catch (\Exception $e) {
      // Log error here
      \Log::debug('HIBP is not working correctly');
    }

    return [
      'breached' => $breached,
      'count' => $count,
    ];
  }

  /**
   * Calculate entropy.
   *
   * @param string $string
   * @return void
   */
  private function calculateEntropy(string $string)
  {
    $length = strlen($string);
    $sizeOfPool = $this->getPool($string);
    $entropy = log(pow($sizeOfPool, $length), 2); // E = log2(RL)
    $score = floor(($entropy / 100) * 4);

    return [
      'value' => $entropy,
      'size_of_pool' => $sizeOfPool,
      'length' => $length,
      'score' => $score > 4 ? 4 : $score,
    ];
  }

  /**
   * Get pool, the number of unique characters from which we generate the password.
   *
   * @return int
   */
  private function getPool(string $string)
  {
    $pool = 0;

    if (preg_match("/[a-zåäö]/", $string)) {
      $pool += 29;
    }

    if (preg_match("/[A-ZÅÄÖ]/", $string)) {
      $pool += 26;
    }

    if (preg_match("/[0-9]/", $string)) {
      $pool += 10;
    }

    if ($this->hasSpecialChar($string)) {
      $pool += strlen($this->specialChars);
    }

    return $pool;
  }

  /**
   * Check if a string has any special chars.
   *
   * @param string $string
   * @return boolean
   */
  private function hasSpecialChar(string $string)
  {
    $specialCharsArray = str_split($this->specialChars);
    $stringArray = str_split($string);
    $intersect = array_intersect($specialCharsArray, $stringArray);

    return !empty($intersect);
  }

  /**
   * Remove swedish chars.
   *
   * @param string $string
   * @return string
   */
  private function removeSwedishChars(string $string)
  {
    $removeChars = ['/å/', '/ä/', '/ö/', '/Å/', '/Ä/', '/Ö/'];
    $replaceWith = ['a', 'a', 'o', 'A', 'A', 'O'];

    return preg_replace($removeChars, $replaceWith, $string);
  }

  /**
   * Undocumented function
   *
   * @param [type] $string
   * @param [type] $char
   * @param [type] $position
   * @return void
   */
  private function stringInsert($string, $char, $position)
  {
    return mb_substr($string, 0, $position) . $char . mb_substr($string, $position);
  }

  /**
   * Undocumented function
   *
   * @param integer $numberOfWords
   * @return array
   */
  private function getWords(int $numberOfWords, $wordLength = 12)
  {
    if ($numberOfWords == 1) {
      $words = DB::select("
        SELECT word FROM words WHERE CHAR_LENGTH(word) >= $wordLength ORDER BY RAND();
      ");
    }

    if ($numberOfWords == 2) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY RAND() LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun1';
      ");
    }

    if ($numberOfWords == 3) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY RAND() LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY RAND() LIMIT 1) AS 'verb';
      ");
    }

    if ($numberOfWords == 4) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY RAND() LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY RAND() LIMIT 1) AS 'verb',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun2';
      ");
    }

    if ($numberOfWords == 5) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY RAND() LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY RAND() LIMIT 1) AS 'verb',
        (SELECT adjective FROM adjectives ORDER BY RAND() LIMIT 1) AS 'adjective2',
        (SELECT singular_indefinite FROM nouns ORDER BY RAND() LIMIT 1) AS 'noun2';
      ");
    }

    return collect($words[0])->values();
  }
}