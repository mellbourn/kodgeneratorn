<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class SystemController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * Import from file.
   *
   * @return void
   */
  public function import()
  {
    DB::table('words')->truncate();

    // Read file
    $json = file_get_contents(base_path('storage/app/words.json'));
    $words = json_decode($json, true);

    // Insert to DB
    foreach (array_chunk($words, 1000) as $chunk) {
        $placeholders = array_fill(0, count($chunk), '(?)');
        DB::insert('INSERT INTO words (word) VALUES ' . join(',', $placeholders), $chunk);
    }

    DB::table('words')->where('word', 'like', '% %')->delete();
    DB::table('words')->where('word', 'like', '%-%')->delete();
  }

  /**
   * Run indexnow process.
   *
   * @return void
   */
  public function indexNow()
  {
    $routeCollection = \Illuminate\Support\Facades\Route::getRoutes();
    $indexableUrls = [];
    foreach ($routeCollection as $route) {
      if ($route->action['namespace'] == 'public') {
        $indexableUrls[] = url('') . '/' . str_replace('/', '', $route->uri());
      }
    }

    $searchEngines = [
      'https://bing.com',
    ];

    foreach ($searchEngines as $searchEngine) {
      $response = Http::post($searchEngine . '/IndexNow', [
        'host' => url(''),
        'key' => config('services.indexnow.api_key'),
        'keyLocation' => url('') . '/indexnow.txt',
        'urlList' => $indexableUrls
      ]);

      echo $searchEngine . ': ' . $response->getStatusCode();
    }
  }
}
