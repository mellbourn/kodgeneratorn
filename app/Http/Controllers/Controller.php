<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * Front page.
   *
   * @return void
   */
  public function index()
  {
    $counts = DB::select("
      SELECT
      (SELECT count(*) FROM adjectives LIMIT 1) AS 'adjectivesCount',
      (SELECT count(*) FROM nouns LIMIT 1) AS 'nounsCount',
      (SELECT count(*) FROM verbs LIMIT 1) AS 'verbsCount',
      (SELECT count(*) FROM words LIMIT 1) AS 'wordsCount';
    ");

    $counts = collect($counts);
    $counts = $counts->first();
    $counts = collect($counts);

    return view('frontpage', $counts);
  }

  /**
   * Analyze page.
   *
   * @return void
   */
  public function testYourPassword()
  {
    return view('test-your-password');
  }

  /**
   * What is a strong password page.
   *
   * @return void
   */
  public function whatIsSecurity()
  {
    return view('what-is-security');
  }

  /**
   * What is a strong password page.
   *
   * @return void
   */
  public function whatIsAStrongPassword()
  {
    return view('what-is-a-strong-password');
  }

  /**
   * How are the passwords created page.
   *
   * @return void
   */
  public function howAreThePasswordsCreated()
  {
    $counts = DB::select("
      SELECT
      (SELECT count(*) FROM adjectives LIMIT 1) AS 'adjectivesCount',
      (SELECT count(*) FROM nouns LIMIT 1) AS 'nounsCount',
      (SELECT count(*) FROM verbs LIMIT 1) AS 'verbsCount',
      (SELECT count(*) FROM words LIMIT 1) AS 'wordsCount';
    ");

    $counts = collect($counts);
    $counts = $counts->first();
    $counts = collect($counts);

    return view('how-are-the-passwords-created', $counts);
  }

  /**
   * Plugins
   *
   * @return void
   */
  public function plugins()
  {
    return view('plugins');
  }

  /**
   * API documenation.
   *
   * @return void
   */
  public function apiDocumentation()
  {
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $apiResponse = Http::get($protocol . $_SERVER['HTTP_HOST'] . '/api/generate');
    $data = $apiResponse->json();

    return view('api-documentation', ['apiResponse' => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)]);
  }

  /**
   * API endpoint
   *
   * @param Request $request
   * @return void
   */
  public function generate(Request $request)
  {
    $passwordService = app()->make('PasswordService');
    $generated = $passwordService->generate($request);

    return response()->json($generated);
  }

  /**
   * API endpoint
   *
   * @param Request $request
   * @return void
   */
  public function analyze(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'password' => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->messages()->first(), 400);
    }

    $passwordService = app()->make('PasswordService');
    $analysis = $passwordService->analyze($request->password);

    return response()->json($analysis);
  }
}
